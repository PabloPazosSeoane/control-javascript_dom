"use strict";

const body = document.querySelector("body");

const formatNum = (num) => {
  return num < 10 ? "0" + num : num;
};

function getHour() {
  let date = new Date();

  let hour = formatNum(date.getHours());
  let minutes = formatNum(date.getMinutes());
  let seconds = formatNum(date.getSeconds());

  body.innerHTML = `<div>${hour} : ${minutes} : ${seconds}</div`;
}

setInterval(getHour, 1000);
