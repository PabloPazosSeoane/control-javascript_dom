"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

console.log(puntuaciones);

function arrayOfPointsGenerator(teams) {
  let totalPointsArray = [];
  for (const team of teams) {
    let totalPoints = 0;
    const { puntos } = team;
    for (const puntuacion of puntos) {
      totalPoints = totalPoints + puntuacion;
    }
    totalPointsArray.push(totalPoints);
  }
  return totalPointsArray;
}

function calculateMinMax(pointsArray) {
  let maxTotalPoints = Math.max(...pointsArray);
  let minTotalPoints = Math.min(...pointsArray);

  let maxIndex = pointsArray.indexOf(maxTotalPoints);
  let minIndex = pointsArray.indexOf(minTotalPoints);

  console.log(
    `El equipo mas puntos que obtuvo fue ${puntuaciones[maxIndex].equipo} con un total de ${maxTotalPoints}`
  );
  console.log(
    `El equipo menos puntos que obtuvo fue ${puntuaciones[minIndex].equipo} con un total de ${minTotalPoints}`
  );
}

function printTeamsAndPoints(teams) {
  let totalPointsArray = arrayOfPointsGenerator(teams);
  calculateMinMax(totalPointsArray);
}

printTeamsAndPoints(puntuaciones);
