"use strict";

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function getUserNum() {
  let num;
  let parsedNum;

  do {
    num = prompt("Introduzca un número del 0 al 100");
    parsedNum = parseInt(num);
  } while (parsedNum < 0 || parsedNum > 100);

  return parsedNum;
}

let numPc = getRandomInt(100);
let numUser = getUserNum();
let isUnlocked = false;

console.log(numPc);
console.log(numUser);

for (let i = 4; i > 0; i--) {
  if (isUnlocked === true) {
    alert(`Felicidades has ganado!`);
  } else {
    if (numPc === numUser) {
      isUnlocked = true;
    } else {
      if (numPc > numUser) {
        alert(
          `El número introducido es menor que la contraseña, te quedan ${i} intentos`
        );
      } else {
        alert(
          `El número introducido es mayor que la contraseña, te quedan ${i} intentos`
        );
      }
      numUser = getUserNum();
    }
  }
}

if (isUnlocked === false) {
  alert(`Se agotaron los intentos :(`);
}
